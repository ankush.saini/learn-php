<!DOCTYPE html>
<html>
<head>
	<title>Hello ankush</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
</head>
<body>

<section>
	<div class="container"> 
		<div class="row">
			<div class="col-md-6 m-auto">
				<h3>Write a factorial program using for loop in php</h3>
				<p><b>Description:</b></p>
				<p>Write a program to calculate factorial of a number using for loop in php.</p> 

				<h3>View Solution/Program</h3>

				<div class="bg-dark text-center text-white mt-3">
				  <?php 
					  $num = 5;
					  $factorial = 1;

					  for ($x=$num; $x>=1; $x--)
					  {
						$factorial = $factorial * $x; 
					  }
					  echo "The factorial of $num is $factorial"; 
				  ?> 
 
				</div>
				 
			</div>
		</div>
	</div>
</section> 


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>