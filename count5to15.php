<!DOCTYPE html>
<html>
<head>
	<title>Hello ankush</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
</head>
<body>

<section>
	<div class="container"> 
		<div class="row">
			<div class="col-md-6 m-auto">
				<h3>Write a program to count 5 to 15 using PHP loop</h3>
				<p><b>Description:</b></p>
				<p>Write a Program to display count, from 5 to 15 using PHP loop as given below.</p>
				<h3>Rules & Hint</h3>
				<ul>
					<li>You can use “for” or “while” loop</li> 
					<li>You can use variable to initialize count</li>
					<li>You can use html tag for line break</li>
				</ul>
				<h3>View Solution/Program</h3>

				<div class="bg-dark text-center text-white mt-3">
					<?php

						$count = 5;

					for ($count; $count <=15 ; $count++) { 
						 
					// 	while($count <= 15) { // same output using while & for loop

							echo $count;
							echo "<br>";
					 
							// $count++;
						}

					?>
				</div>
			</div>
		</div>
	</div>
</section> 


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>