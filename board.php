<!DOCTYPE html>
<html>
<head>
	<title>Hello ankush</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
</head>
<body>

<section>
	<div class="container"> 
		<div class="row">
			<div class="col-md-6 m-auto pl-2">
				<h3>Write a Factorial program in PHP using recursive function</h3>
			 
				<p>View Solution/Program</p>

				<div class="bg-dark text-white mt-3">
                
 
                    <table>

                    <?php  

                        for($row = 1; $row<=10; $row++){
                            echo "<tr>";
                            for($column=1;$column<=8;$column++)
                            {
                               
                               $total=$row+$column;
                               if($total%2==0)
                               {
                                   echo "<td height=35px width=35px bgcolor=#FFFFFF></td>";
                                   
                               }
                               else
                               {
                                   echo "<td height=35px width=35px bgcolor=#000000></td>";
                               }
                                
                            }
                            echo "</tr>";
                        }
                        ?>
                    </table>
                    
                      
				</div>
               
				 
			</div>
             
		</div>
	</div>
</section> 


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>