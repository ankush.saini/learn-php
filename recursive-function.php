<!DOCTYPE html>
<html>
<head>
	<title>Hello ankush</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
</head>
<body>

<section>
	<div class="container"> 
		<div class="row">
			<div class="col-md-6 m-auto pl-2">
				<h3>Write a Factorial program in PHP using recursive function</h3>
			 
				<p>View Solution/Program</p>

				<div class="bg-dark text-center text-white mt-3">
                 <?php 
                  
					  function factorial($num) {
                          if($num<2){
                              return 1;
                          }else {
                              return($num * factorial($num-1));
                          }
                        }
                        echo factorial(5);
				  ?> 
  
				</div>
               
				 
			</div>
           
		</div>
	</div>
</section> 


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>